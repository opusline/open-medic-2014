﻿CREATE TABLE openmedic.REF_AGE
(
  AGE character(2),
  LABEL_AGE character(11),
  CONSTRAINT AGE_pkey PRIMARY KEY (AGE)
);

CREATE INDEX REF_AGE_index ON openmedic.REF_AGE (AGE);