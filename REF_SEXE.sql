﻿CREATE TABLE openmedic.REF_SEXE
(
  SEXE character(1),
  LABEL_SEXE character(15),
  CONSTRAINT SEXE_pkey PRIMARY KEY (SEXE)
);

CREATE INDEX REF_SEXE_index ON openmedic.REF_SEXE (SEXE);