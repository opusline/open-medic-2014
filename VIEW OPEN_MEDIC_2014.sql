﻿CREATE VIEW openmedic.vw_open_medic_2014 AS
SELECT 	A.atc1, 
	B.label_atc1,
	A.atc2, 
	C.label_atc2,
	A.atc3, 
	D.label_atc3,
	A.atc4, 
	E.label_atc4,
	A.atc5, 
	F.label_atc5,
	A.cip13, 
	A.top_gen, 
	G.label_top_gen,
	A.gen_num, 
	H.label_gen_num,
	A.age, 
	I.label_age,
	A.sexe, 
	J.label_sexe,
	A.ben_reg, 
	K.label_ben_reg,
	A.psp_spe, 
	L.label_spe,
	L.type_spe,
	A.boites, 
	A.rem, 
	A.bse
FROM openmedic.open_medic_2014 AS A
LEFT JOIN openmedic.ref_atc1 AS B
	ON A.atc1 = B.atc1
LEFT JOIN openmedic.ref_atc2 AS C
	ON A.atc2 = C.atc2
LEFT JOIN openmedic.ref_atc3 AS D
	ON A.atc3 = D.atc3
LEFT JOIN openmedic.ref_atc4 AS E
	ON A.atc4 = E.atc4
LEFT JOIN openmedic.ref_atc5 AS F
	ON A.atc5 = F.atc5
LEFT JOIN openmedic.ref_top_gen AS G
	ON A.top_gen = G.top_gen
LEFT JOIN openmedic.ref_gen AS H
	ON A.gen_num = H.gen_num
LEFT JOIN openmedic.ref_age AS I
	ON A.age = I.age
LEFT JOIN openmedic.ref_sexe AS J
	ON A.sexe = J.sexe
LEFT JOIN openmedic.ref_reg AS K
	ON A.ben_reg = K.ben_reg
LEFT JOIN openmedic.ref_spe AS L
	ON A.psp_spe = L.psp_spe