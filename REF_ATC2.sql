﻿CREATE TABLE openmedic.REF_ATC2
(
  ATC2 character(3),
  LABEL_ATC2 character(75),
  CONSTRAINT ATC2_pkey PRIMARY KEY (ATC2)
);

CREATE INDEX REF_ATC2_index ON openmedic.REF_ATC2 (ATC2);