﻿CREATE TABLE openmedic.REF_REG
(
  BEN_REG character(2),
  LABEL_BEN_REG character(36),
  CONSTRAINT REG_pkey PRIMARY KEY (BEN_REG)
);

CREATE INDEX REF_REG_index ON openmedic.REF_REG (BEN_REG);