﻿CREATE TABLE openmedic.REF_SPE
(
  PSP_SPE character(2),
  LABEL_SPE character(45),
  TYPE_SPE character(22),
  CONSTRAINT SPE_pkey PRIMARY KEY (PSP_SPE)
);

CREATE INDEX REF_SPE_index ON openmedic.REF_SPE (PSP_SPE);