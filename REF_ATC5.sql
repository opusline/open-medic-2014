﻿CREATE TABLE openmedic.REF_ATC5
(
  ATC5 character(7),
  LABEL_ATC5 character(77),
  CONSTRAINT ATC5_pkey PRIMARY KEY (ATC5)
);

CREATE INDEX REF_ATC5_index ON openmedic.REF_ATC5 (ATC5);