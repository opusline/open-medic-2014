﻿CREATE TABLE openmedic.REF_ATC1
(
  ATC1 character(1),
  LABEL_ATC1 character(75),
  CONSTRAINT ATC1_pkey PRIMARY KEY (ATC1)
);

CREATE INDEX REF_ATC1_index ON openmedic.REF_ATC1 (ATC1);