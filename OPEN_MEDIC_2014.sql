﻿CREATE TABLE openmedic.OPEN_MEDIC_2014
(
  ATC1 character(1),
  ATC2 character(3),
  ATC3 character(4),
  ATC4 character(5),
  ATC5 character(7),
  CIP13 character(13),
  TOP_GEN character(1),
  GEN_NUM smallint,
  AGE character(2),
  SEXE character(1),
  BEN_REG character(2),
  PSP_SPE character(2),
  BOITES integer,
  REM real,
  BSE real
);

CREATE INDEX ATC1_index ON openmedic.OPEN_MEDIC_2014 (ATC1);
CREATE INDEX ATC2_index ON openmedic.OPEN_MEDIC_2014 (ATC2);
CREATE INDEX ATC3_index ON openmedic.OPEN_MEDIC_2014 (ATC3);
CREATE INDEX ATC4_index ON openmedic.OPEN_MEDIC_2014 (ATC4);
CREATE INDEX ATC5_index ON openmedic.OPEN_MEDIC_2014 (ATC5);
CREATE INDEX CIP13_index ON openmedic.OPEN_MEDIC_2014 (CIP13);
CREATE INDEX TOP_GEN_index ON openmedic.OPEN_MEDIC_2014 (TOP_GEN);
CREATE INDEX AGE_index ON openmedic.OPEN_MEDIC_2014 (AGE, CIP13);
CREATE INDEX BEN_REG_index ON openmedic.OPEN_MEDIC_2014 (BEN_REG, CIP13);
CREATE INDEX PSP_SPE_index ON openmedic.OPEN_MEDIC_2014 (PSP_SPE, CIP13);