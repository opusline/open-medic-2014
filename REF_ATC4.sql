﻿CREATE TABLE openmedic.REF_ATC4
(
  ATC4 character(5),
  LABEL_ATC4 character(93),
  CONSTRAINT ATC4_pkey PRIMARY KEY (ATC4)
);

CREATE INDEX REF_ATC4_index ON openmedic.REF_ATC4 (ATC4);