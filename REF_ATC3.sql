﻿CREATE TABLE openmedic.REF_ATC3
(
  ATC3 character(4),
  LABEL_ATC3 character(88),
  CONSTRAINT ATC3_pkey PRIMARY KEY (ATC3)
);

CREATE INDEX REF_ATC3_index ON openmedic.REF_ATC3 (ATC3);